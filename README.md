# Petcube<->Server Integration Demo

## Install & Run Server 

For running server like other services (e.g. nginx, mysl) we need to install System_Daemon ext:

`pear install -f System_Daemon`

Then:

```cd /<Project dir>```

```git clone https://Hardstyle_R@bitbucket.org/Hardstyle_R/petcubetest.git```

```chmod a+x ./SocketCli.php```

```./SocketCli.php```

It will create init.d file. So we can run it by:

```sudo /etc/init.d/SocketCli start```

Stop: ```sudo /etc/init.d/SocketCli stop```

If it still running (Check ```ps uf -C SocketCli.php ```), U can simply kill him by typing:

```killall -9 SocketCli.php```

You obviusly could also run server script directly without System_Daemon:

```php -q ./SocketServer.php```

## Client

To run a client and starting to listen commands from server:

`php -q /cube/CubeSimple.php` <- This is more complicated version of Cube Demo

--OR--
`php -q /cube/index.php`


### Exmplanation of concept
There are a few classes hierarchy. 
The main code that actually run Cube looks like:


`$cube = new CubeSimple();`



```$cube->addObserver( new AuthAction(), CubeSimple::EVENT_MESSAGE_AUTH);```

```$cube->addObserver( new KeepAliveAction(), CubeSimple::EVENT_MESSAGE_KEEP_ALIVE);```

```$cube->addObserver( new LaserOnAction(), CubeSimple::EVENT_MESSAGE_LASER_ON);```



```$cube->run();```
```$cube->end();```

The magic is within the run method when the Cube listen (reads socket) and if there are some messages from Server - doing smth...

If We need to respond for some Server message (a new type of action), receive params and doing something accordingly to this message(action), 
we just simpy will add a new Obserever (Event Handler):

```$cube->addObserver( new LaserOnAction(), CubeSimple::EVENT_MESSAGE_LASER_ON);```

LaserOnAction class should implement notify method, within needed block of code. 
And when the event/message/action, that we've been subscribed, will fire - that piece of code will run.

#### AuthAction
Implements the basic authentification functionality
#### KeepAliveAction
Reacts to server keepAlive message
#### LaserOnAction
Another example how we could do smth when the server ask for some type of action

## API

Cube class has some fetchServerHost() method that actually can be used in case if we have more than one server instances.
It can encapsylate a logic of fetching a new Socket IP and Port, by calling some API method (http://api.petcube.dev/v1/socket_server/available_address)

The function of API is also to be a broker beetwen a devices (phones, web apps) and cubes. 
An example of case would be: if we push the button on phone screen to see if cube is enabled or to fire some action on it (move camera to the left or etc),
we would do the POST call to API, that in turn pass this action to needed cube(s)


#### Actions Storage and getCurrentCubeAction
There is a simple MySQL db table that store actions got from phones, system or simply hardtyped by admin

Every Server loop script call getCurrentCubeAction function and grab an actions for concrete cube-client. 
If action exists it will make a call to related cube-client.

In these example I just use MySQL table. In real system it better to use some key-store Database like Redis, Memcache, 
RebbitMQ (there should be some kinf of action queues)

## DB

Run script from db folder and insert needed data









