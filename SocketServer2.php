#!/usr/bin/env php
<?php
error_reporting(E_ALL);

/* Allow the script to wait for connections. */
set_time_limit(0);

/* Turn on implicit output dump*/
ob_implicit_flush();

$address = '192.168.1.144';
$port = 6889;

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}

if (socket_bind($sock, $address, $port) === false) {
    echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

if (socket_listen($sock, 5) === false) {
    echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
}

//clients array
$clients = array();

do {
    $read = array();
    $read[] = $sock;
    
    $read = array_merge($read,$clients);
    
    // Set up a blocking call to socket_select
    if(socket_select($read,$write = NULL, $except = NULL, $tv_sec = 5) < 1)
    {
        //    SocketServer::debug("Problem blocking socket_select?");
        continue;
    }
    
    // Handle new Connections
    if (in_array($sock, $read)) {        
        
        if (($msgsock = socket_accept($sock)) === false) {
            echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
            break;
        }
        $clients[] = $msgsock;
        $key = array_keys($clients, $msgsock);
        /* Welcome message and instructions */
        $msg = "\nWelcome to the Petcube Test Server. \n" .
            "You are the customer number: {$key[0]}\n" .
            "To exit, type 'quit'. To shut down the server type 'shutdown'. \n";
        socket_write($msgsock, $msg, strlen($msg));

    }
    
    // Handle Input
    foreach ($clients as $key => $client) { // for each client        
        if (in_array($client, $read)) {
            if (false === ($buf = socket_read($client, 2048, PHP_NORMAL_READ))) {
                echo "socket_read () failed: reason:" . socket_strerror(socket_last_error($client)) . "\n";
                break 2;
            }

            //http://stackoverflow.com/questions/3996215/how-to-detect-client-disconnection-on-php-socket-listener
            if($buf === FALSE) {
                socket_close($client);
                echo 'Client disconnected!',"\r\n";
                continue;
            }

            if (!$buf = trim($buf)) {
                continue;
            }
            if ($buf == 'quit') {
                unset($clients[$key]);
                socket_close($client);
                break;
            }
            if ($buf == 'shutdown') {
                socket_close($client);
                break 2;
            }

            //$msg = "Hey {$key}! I said: action=laserOn\n";
            //socket_write($client, $msg, strlen($msg));

            $talkback = "Client {$key}: U said '". strtoupper($buf)."'.\n";
            socket_write($client, $talkback, strlen($talkback));
            echo strtoupper($buf)."\n";
        }
        
    }        
} while (true);

socket_close($sock);
?>
