# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.31-0ubuntu0.12.04.2)
# Database: test
# Generation Time: 2013-09-06 11:43:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cubes_actions_stack
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cubes_actions_stack`;

CREATE TABLE `cubes_actions_stack` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cube_id` varchar(1024) DEFAULT NULL,
  `action` varchar(256) DEFAULT NULL,
  `params` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cubes_actions_stack` WRITE;
/*!40000 ALTER TABLE `cubes_actions_stack` DISABLE KEYS */;

INSERT INTO `cubes_actions_stack` (`id`, `cube_id`, `action`, `params`)
VALUES
	(1,'777','Laser','s:2:\"On\";'),
	(2,'999','OtherAction','a:2:{s:6:\"param1\";s:4:\"val1\";s:6:\"param2\";s:4:\"val2\";}');

/*!40000 ALTER TABLE `cubes_actions_stack` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cubes_phones_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cubes_phones_relations`;

CREATE TABLE `cubes_phones_relations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cube_id` int(11) DEFAULT NULL,
  `phone_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `cubes_phones_relations` WRITE;
/*!40000 ALTER TABLE `cubes_phones_relations` DISABLE KEYS */;

INSERT INTO `cubes_phones_relations` (`id`, `cube_id`, `phone_id`)
VALUES
	(1,777,234),
	(2,999,235);

/*!40000 ALTER TABLE `cubes_phones_relations` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
