<?php
error_reporting(1);
ini_set('display_errors', '1');

/* Allow the script to wait for connections. */
set_time_limit(0);



interface IObservable
{
    public function addObserver( IObserver $objObserver, $strEventType );
    public function fireEvent( $strEventType );
}

interface IObserver
{
    public function notify( IObservable $objSource, $objArguments );
}

/**
 * Simple Observer|EventHandler realization
 */
class CubeAbstract implements IObservable {

    protected $_observers;

    public function addObserver( IObserver $objObserver, $strEventType )
    {
        $this->_observers[$strEventType][] = $objObserver;
    }

    public function fireEvent( $strEventType )
    {
        $this->e('Firing event: '. $strEventType);
        if( is_array( $this->_observers[$strEventType] ) )
        {
            foreach ( $this->_observers[$strEventType] as $objObserver )
            {
                $objObserver->notify( $this, $strEventType );
            }
        }
    }
}


class CubeSimple extends CubeAbstract {

    public $serverAPIUrl = null;
    public $serverAddress = '192.168.1.144';
    public $serverPort = 6839;

    const SERVER_API_URL = 'http://api.petcube.dev/v1/';
    const SERVER_API_SOCKET_CONN_ADDRESS = '/socket_server/available_address';

    const DEFAULT_SERVER_ADDRESS = '192.168.1.144';
    const DEFAULT_SERVER_PORT = 6869;

    const EVENT_MESSAGE_AUTH = 'Cube-Authentication';
    const EVENT_MESSAGE_KEEP_ALIVE = 'KeepAlive';
    const EVENT_MESSAGE_LASER_ON = 'Cube-Action: Laser=On';

    public $reconnectTimeout = 20; //seconds to get new connection

    public $errors = null;
    public $debug = true;

    private $_socket = null;

    public function __construct() {
        error_reporting(1);
        ini_set('display_errors', '1');

        /* Allow the script to wait for connections. */
        set_time_limit(0);

        /* Turn on implicit output dump*/
        ob_implicit_flush(1);

        $this->setServerAPIUrl(self::SERVER_API_URL); // this will call to api and get available ip and port for the connection
        $this->fetchServerHost();
    }

    /**
     * Manual set server addresses
     * @param $url
     */
    public function setServerAPIUrl($url) {
        if (!empty($url)) {
            $this->serverAPIUrl = $url;
        }
        $this->fetchServerHost();
    }

    /**
     * Make call to Server API to get available app IP and Port
     * @return array
     */
    private function fetchServerHost() {
        if (isset($this->serverAPIUrl)) {
            $apiMethodUri = self::SERVER_API_SOCKET_CONN_ADDRESS;
            //make curl request to server and get available server address

            $response = array(
                'address' => '192.168.1.144',
                'port' => 6869
            );

            $this->serverAddress = $response['address'];
            $this->serverPort = $response['port'];
            return $response;
        }
        else {
            $this->serverAddress = self::DEFAULT_SERVER_ADDRESS;
            $this->serverPort = self::DEFAULT_SERVER_PORT;
        }
    }

    public function getServerAddress() {
        return $this->serverAddress;
    }

    public function getServerPort() {
        return $this->serverPort;
    }

    public function e($str, $level = E_ERROR) {
        if ($this->debug) {
            if ($level == E_ERROR ) {
                $this->errors[] = $str;
            }
            echo $str . "\r\n";
        }
    }

    /**
     * Get Current Socket Resource
     *
     * @return bool|null|resource
     */
    public function getSocket() {

        if (!isset($this->_socket) || !$this->_socket) {
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if ($socket === false) {
                $this->e("Cannot create socket. Reason: " . socket_strerror(socket_last_error()));
                $this->_socket = false;
            }
            else {
                $this->e("Trying to connect with " . $this->serverAddress . " at port " . $this->serverPort . "...", E_NOTICE);
                $result = socket_connect($socket, $this->serverAddress, $this->serverPort);
                if ($result === false) {
                    $this->e("Cannot connect to socket.\nReason: ($result) " . socket_strerror(socket_last_error($socket)));
                    $this->e("Closing socket...", E_NOTICE);
                    socket_close($socket);
                    $this->_socket = false;
                } else {
                    $this->_socket = &$socket;
                }
            }

        }
        return $this->_socket;

    }

    /**
     * Main method to connect and observe of Server
     */
    public function run() {

        //Observe the server
        while (true) {
            $socket = $this->getSocket();
            if ($socket !== false) {
                socket_set_nonblock($socket);
                $out = @socket_read($socket, 65535, PHP_NORMAL_READ);
                if ($out === FALSE) {
                    $this->e("Connection closed.", E_NOTICE);

                    //try to establish connection again
                    $this->e('Retrying to connect...', E_NOTICE);
                    $this->_socket = null;
                    sleep($this->reconnectTimeout);
                    if (!$this->getSocket()) {
                        break;
                    }
                    break;
                }

                if (trim($out) != '') {
                    //$this->e('There is smth to process...'. (string)trim($out));
                    //process server message
                    if (strstr(trim($out), self::EVENT_MESSAGE_AUTH)) {
                        $this->fireEvent(self::EVENT_MESSAGE_AUTH);
                    }
                    if (strstr(trim($out), self::EVENT_MESSAGE_KEEP_ALIVE)) {
                        $this->fireEvent(self::EVENT_MESSAGE_KEEP_ALIVE);
                    }
                    if (strstr(trim($out), self::EVENT_MESSAGE_LASER_ON)) {
                        $this->fireEvent(self::EVENT_MESSAGE_LASER_ON);
                    }

                    continue;
                }
                else {
                    //send smth to server...
                }

            }
            else {
                break;
            }

        }
        $this->end();

    }

    public function end() {
        $socket = $this->_socket;
        if (isset($socket) && $socket) {
            $this->e("Closing socket...", E_NOTICE);
            socket_close($socket);
        }
    }

    public function __destruct() {
        $this->e('Destroying cube...', E_NOTICE);
        $this->end();
    }

}

/**
 * Class AuthAction
 */
class AuthAction implements IObserver
{
    /**
     * @var null|CubeSimple
     */
    private $_cube = null;

    public function notify( IObservable $objSource, $strEventType )
    {
        if( strstr($strEventType, CubeSimple::EVENT_MESSAGE_AUTH) && $objSource instanceof CubeAbstract )
        {
            $this->_cube = $objSource;
            $this->authenticate();
        }
    }

    public function authenticate() {
        if (isset($this->_cube)) {
            $in = 'Cube-Authenticate: Cube-Key=' . '777;Cube-Secret=base64string' . "\r\n";
            $this->_cube->e("Doing Auth Request: ". $in, E_NOTICE);
            if (socket_write($this->_cube->getSocket(), $in, strlen($in))) {
                $authenticated = true;
                $this->_cube->e("Auth sent", E_NOTICE);
            }
            else {
                $this->_cube->e("Couldn't send Auth request on server");
                unset($this->_cube);
            }
        }
    }

}

class KeepAliveAction implements IObserver {

    public function notify( IObservable $objSource, $strEventType )
    {
        if( strstr($strEventType, CubeSimple::EVENT_MESSAGE_KEEP_ALIVE) && $objSource instanceof CubeAbstract )
        {
            /**
             * @var $objSource CubeSimple
             */
            $objSource->e("Server wants to keep me ALIVE!", E_NOTICE);
        }
    }
}

/**
 * If Server wants to enable Cube's laser
 * LaserOnAction
 */
class LaserOnAction implements IObserver {

    public function notify( IObservable $objSource, $strEventType )
    {
        if( strstr($strEventType, CubeSimple::EVENT_MESSAGE_LASER_ON) && $objSource instanceof CubeAbstract )
        {
            /**
             * @var $objSource CubeSimple
             */
            $objSource->e("Server wants to know if my LASER is ENABLED", E_NOTICE);
            $this->_cube = $objSource;
            $this->doLaserOn();
        }
    }

    public function doLaserOn()
    {
        if (isset($this->_cube)) {
            $in = 'Cube-Action-Response: Laser=Enabled'. "\r\n";
            $this->_cube->e("Doing LaserOn Response to server: " . $in, E_NOTICE);
            if (socket_write($this->_cube->getSocket(), $in, strlen($in))) {
                $authenticated = true;
                $this->_cube->e("LaserOn Response sent", E_NOTICE);
            } else {
                $this->_cube->e("Couldn't send LaserOn Response on server");
                unset($this->_cube);
            }
        }
    }
}



$cube = new CubeSimple();
$cube->serverPort = 6869;
$socket = $cube->getSocket();

$cube->addObserver( new AuthAction(), CubeSimple::EVENT_MESSAGE_AUTH);
$cube->addObserver( new KeepAliveAction(), CubeSimple::EVENT_MESSAGE_KEEP_ALIVE);
$cube->addObserver( new LaserOnAction(), CubeSimple::EVENT_MESSAGE_LASER_ON);


$cube->run();
$cube->end();


