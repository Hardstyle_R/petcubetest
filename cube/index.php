
<?php

$debug = true;
function e($str)
{
    global $debug;
    if ($debug) {
        //exec("clear");
        echo $str . "\r\n";

        //sleep(1);
        //while ($lines--) echo " \n";
        //ob_flush();
        //flush();
        //sleep(1);
        //usleep(10000);
        //$fd = fopen("/var/www/petcube.dev/cube/log.log", "w");
        //$str  = $str . "\r\n";
        //fputs($fd, $str, strlen($str));
        //usleep(10000);
    }
}

error_reporting(1);
ini_set('display_errors', '1');

/* Allow the script to wait for connections. */
set_time_limit(0);

/* Turn on implicit output dump*/
ob_implicit_flush(1);


e("Petcube Client TCP/IP connectivity test");

/* Получаем порт сервиса WWW. */
$service_port = 6869; //getservbyname('www', 'tcp');

/* Получаем  IP адрес целевого хоста. */
$address = '192.168.1.144'; //gethostbyname('www.example.com');


//while(ob_get_level()) { ob_end_flush(); }

/* Создаём  TCP/IP сокет. */
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    e("Cannot do socket_create(): reason: " . socket_strerror(socket_last_error()));
} else {
    //e("OK");
}

e("Trying to connect with '$address' at port '$service_port'...");
$result = socket_connect($socket, $address, $service_port);
if ($result === false) {
    e("Cannot do socket_connect().\nReason: ($result) " . socket_strerror(socket_last_error($socket)));
    e("Closing socket...");
    socket_close($socket);
    exit();
} else {
    e("OK");

}


//set non-blocking mode
//if (socket_set_nonblock($socket) === false) {
//    echo "socket_set_nonblock() failed. Reason: ". socket_strerror(socket_last_error($socket));
//}

//while($buf = @socket_read($socket, 65536, PHP_BINARY_READ)) {
//    $response.= $buf;
//    if ($buf === false || empty($buf) || $buf == '' || $buf == ' ') { //|| $buf == "\r\n" || $buf == "\n") {
//        e('break');
//        break;
//    }
//    e('Responce: '. $response);
//
//}


//Welcome message
//e('Responce: '. $response);

//infinite waiting for server
while (true) {

    $out = @socket_read($socket, 65535, PHP_NORMAL_READ);

    //e('Debug: '.$out);
    if ($out === FALSE) {
        e("Connection closed.");
        break;
    }

    if (trim($out) != '') {
        //There are some input form server...

        e("I've got a message: ". trim($out));

        //We need to path credentials to server!
        if (!$authenticated && strstr($out, 'Cube-Authentication')) {
            //$in = 'username=cube_id' . '777' . "\r\n";// . (string)rand(1, 2000) . "\r\n";
            $in = 'Cube-Authenticate: Cube-Key=' . '777;Cube-Secret=base64string' . "\r\n";
            e("Doing Auth Request: ". $in);
            if (socket_write($socket, $in, strlen($in))) {
                $authenticated = true;
                e("OK");
            }
            else {
                e("Не вышло отправить запрос на сервер");
//                socket_close($socket);
//                exit();
                break;
            }
        }
        continue;

    }
    else {
        //smth else

        //
        if (in_array(trim(fgets(STDIN)), array('q', 'quit', 'exit'))) {
            $msg = 'exit' . "\r\n";
            socket_write($socket, $msg, strlen($msg));
            e("Хорошо, выходим. Закрываем сокет...");
            socket_close($socket);
            exit;
        }
        else {
            $msg = trim(fgets(STDIN));
            if (!empty($msg) && $msg != "\r\n" && $msg != "\n") {
                $msg.= "\r\n";
                e("Sending message: ". $msg);
                //sleep(1);
                socket_write($socket, $msg, strlen($msg));
            }

        }

    }



//    ob_flush();
//    flush();
//    sleep(1);
}


e("Closing socket...");
socket_close($socket);
e("OK");

