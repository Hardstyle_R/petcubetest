#!/usr/bin/php -q
<?php

/**
 * Server is actually a broker between cubes, API and Phones related to this cubes
 */

$debug = true;
$keepAlivePingTimeout = 5;
$keepAlivePingLoopsNumber = 10;
$max_clients = 5;

define("CUBE_ENABLE_AUTH", true);

function e($str)
{
    global $debug;
    if ($debug) {
        echo($str . "\n");
    }
}

/**
 * Get action for cube with $userName id from some storage if exists
 * @param $userName
 * @return array
 */
function getCurrentCubeAction($userName) {

    //@todo connect and select actions from db by userName

    mysql_connect("localhost", "root", "hardstyler") or die(mysql_error());
    mysql_select_db("test") or die(mysql_error());

    $query = "SELECT * FROM `cubes_actions_stack` WHERE `cube_id` = ". (string)$userName .";";
    $result = mysql_query($query)
        or die(mysql_error());

    while($row = mysql_fetch_array( $result )) {
        $action['cube_id'] = $row['cube_id'];
        $action['action'] = $row['action'];
        $action['params'] = unserialize($row['params']);
        $actions[] = $action;
    }

    $return = array();
    if (isset($actions[0]['action']) && isset($actions[0]['params'])) {
        $return = array(
            'action' => $actions[0]['action'], // action name
            'params' => is_array($actions[0]['params']) ? $actions[0]['params'] : (string)$actions[0]['params']
        );
    }
//    return array(
//        'action' => 'Laser', // action name
//        'params' => 'On'// array()
//    );
    return $return;
}

/**
 * Format action message before send
 * @param $actionName
 * @param $actionParams
 * @return string
 */
function formatAction($actionName, $actionParams) {
    $actionHeader = "Cube-Action:";
    $return = '';
    if (!is_array($actionParams)) {
        $return = $actionHeader . " ". $actionName . "=" . (string)$actionParams;
    }
    return $return;
}

e("Starting...");
error_reporting(1);
ini_set('display_errors', '1');

e("Creating master socket...");
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

//set non-blocking mode
if (socket_set_nonblock($socket) === false) {
    echo "socket_set_nonblock() failed. Reason: ".socket_strerror(socket_last_error($socket));
}

e("Setting socket options...");
socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
e("Binding socket...");
socket_bind(
    $socket,
    "192.168.1.144",
    6869
);
e("Listening...");
socket_listen($socket, $max_clients);

$clients = array('0' => array('socket' => $socket));

while (TRUE) {
    e("Beginning of WHILE");
    $read[0] = $socket;

    for ($i = 1; $i < count($clients) + 1; ++$i) {
        if ($clients[$i] != NULL) {
            $read[$i + 1] = $clients[$i]['socket'];
        }
    }

    e("Selecting socket...");
    //$ready = socket_select($read, $write = NULL, $except = NULL, $tv_sec = NULL);
    $ready = socket_select($read,$write = NULL, $except = NULL, $tv_sec = 15);
    e("socket_select returned " . $ready);
    e("If...");
    //var_dump($socket);
    //var_dump($read);
    if (in_array($socket, $read)) {
        e("If OK");
        for ($i = 1; $i < $max_clients + 1; ++$i) {
            if (!isset($clients[$i])) {

                //set non-blocking
                if (socket_set_nonblock($socket) === false) {
                    echo "socket_set_nonblock() failed. Reason: ".socket_strerror(socket_last_error($socket));
                }

                e("Accepting connection...");
                $clients[$i]['socket'] = socket_accept($socket);

                socket_getpeername($clients[$i]['socket'], $ip);
                e("Peer: " . $ip);
                $clients[$i]['ipaddr'] = $ip;


                //do the client auth or send him out
                if (CUBE_ENABLE_AUTH && !isset($clients[$i]['authenticated']) || $clients[$i]['authenticated'] === false) {
                    e("Doing auth...");

                    if (!isset($clients[$i]['auth_header_sent']) || empty($clients[$i]['auth_header_sent'])) {
                        e('Auth. Sending auth header');
                        //send "need auth" header:
                        $msg = "Cube-Authentication: Simple" . "\r\n";
                        if(socket_write($clients[$i]['socket'], $msg, strlen($msg))) {
                            $clients[$i]['auth_header_sent'] = true;
                        }
                        else {
                            e("Can't write to sock for auth. Break the loop...");
                            break;
                        }
                    }
                    else {
                        ///
                    }

                    e('after auth write...');
                    //than read response for some period, and if no data - close connection
                    $timeStarted = time();
                    $data = '';
                    $buf = '';
                    while($buf = @socket_read($clients[$i]['socket'], 1024, PHP_NORMAL_READ)) {
                        //e("Auth. Reading... ". $buf);
                        $data .= $buf;
                        if (time() - $timeStarted >= 10 || empty($buf) || in_array(trim($buf), array("\r\n", "\n", '', " ")) || $buf === false) {
                            break;
                        }
                    }

                    //do check auth info
                    //e('Auth. Checking');
                    if (!empty($data)) {
//                        $params = explode('=', $data);
//                        if (isset($params[1]) && (trim($params[1]) == "cube_id777")) {
//                            e('Client ' .$clients[$i]['ipaddr'] . ' been authenticated!');
//                            $msg = "You've been successfully authenticated!". "\r\n";
//                            $clients[$i]['authenticated'] = true;
//                            socket_write($clients[$i]['socket'], $msg, strlen($msg));
//                        }
                        e('d'. $data);
                        $params = explode(':', $data);
                        if (isset($params[1])) {

                            //parse params
                            e('Params[1]: '. $params[1]. "\r\n");
                            foreach (explode(';', trim($params[1])) as $el) {
                                $credential = explode('=', $el);
                                if (isset($credential[0])) {
                                    $credentials[$credential[0]] = isset($credential[1]) ? $credential[1] : null;
                                }
                            }

                            //check if it's our client
                            if (isset($credentials['Cube-Key']) && $credentials['Cube-Key'] == '777') {
                                $clients[$i]['username'] = 777;
                                e('Client ' .$clients[$i]['ipaddr'] . ' been authenticated!');
                                $msg = "You've been successfully authenticated!". "\r\n";
                                $clients[$i]['authenticated'] = true;
                                socket_write($clients[$i]['socket'], $msg, strlen($msg));
                            }
                            else {
                                e('Got no one auth key from client ' .$clients[$i]['ipaddr'] . 'Skipping...');
                                $msg = "Auth failed. You're a bad guy!". "\r\n";
                                socket_write($clients[$i]['socket'], $msg, strlen($msg));
                                socket_close($clients[$i]['socket']);
                                unset($clients[$i]);
                                break;
                            }
                        }
                    } else {
                        //
                        e('Client ' .$clients[$i]['ipaddr'] . ' has not been authenticated! Skipping...');
                        $msg = "Auth failed. You're a bad guy!". "\r\n";
                        socket_write($clients[$i]['socket'], $msg, strlen($msg));
                        socket_close($clients[$i]['socket']);
                        unset($clients[$i]);
                        break;
                    }



                }

                $msg = 'Welcome to test Petcube Socket Server' . "\n";
                //socket_write($clients[$i]['socket'], $msg, strlen($msg));
                $msg .= 'There are ' . (count($clients) - 1) . ' client(s) connected to this server.' . "\r\n";
                socket_write($clients[$i]['socket'], $msg);
                $msg = " ";
                socket_write($clients[$i]['socket'], $msg, strlen($msg));

                echo 'New client connected: ' . $clients[$i]['ipaddr'] . ' ';
                break;
            } elseif ($i == $max_clients - 1) {
                echo 'Too many Clients connected!' . "\r\n";
            }

            if (--$ready <= 0) {
                continue;
            }
        }
    }

    e("For...");
    for ($i = 1; $i < $max_clients + 1; ++$i) {
        e("In...");
        if (in_array($clients[$i]['socket'], $read)) {
            e("Reading data...");
            $data = @socket_read($clients[$i]['socket'], 1024, PHP_NORMAL_READ);

            e("Data been read..." . $data);

            if ($data === FALSE) {
                //http://stackoverflow.com/a/4012657
                socket_close($clients[$i]['socket']);
                unset($clients[$i]);
                echo 'Client disconnected!',"\r\n";
                continue;
            }

            $data = trim($data);

            if (!empty($data)) {
                if ($data == 'exit' || $data == 'quit') {
                    socket_write($clients[$i]['socket'], 'Thanks for trying my test Petcube Socket Server. Bye!' . "\n");
                    echo 'Client ', $i, ' is exiting.', "\n";
                    socket_close($clients[$i]['socket']);
                    unset($clients[$i]);
                    continue;
                }

                //When a one client write smthing, message will be send each other clients
                for ($j = 1; $j < $max_clients + 1; ++$j) {
                    if (isset($clients[$j]['socket'])) {
                        if (($clients[$j]['socket'] != $clients[$i]['socket']) && ($clients[$j]['socket'] != $socket)) {
                            socket_write($clients[$j]['socket'], '[' . $clients[$i]['ipaddr'] . '] says: ' . strtoupper($data) . "\r\n");
                            echo($clients[$i]['ipaddr'] . ' is sending a message!' . "\r\n");
                        }
                    }
                }
                break;
            }
        }
    }
    $time = time();

    //define first time for keepAlive
//    foreach ($clients as $k => $client) {
//        if (!isset($clients[$k]['lastKeepAlivePingTime']) || empty($clients[$k]['lastKeepAlivePingTime'])) {
//            $clients[$k]['lastKeepAlivePingTime'] = $time;
//        }
//    }


    /**
     * Sending ping to clients for not going out
     */
    if ($loops == 0) {
        $firstloop = time();
        $loops++;

    } else {
        $loops++;
        //if ((time() - $firstloop) >= $keepAlivePingTimeout && $loops > $keepAlivePingLoopsNumber) {

            for ($j = 1; $j < $max_clients + 1; ++$j) {
                if (isset($clients[$j]['socket'])) {
                    if ($clients[$j]['socket'] != $socket) {

                        if (!isset($clients[$j]['loops']) || empty($clients[$j]['loops'])) {
                            $clients[$j]['lastKeepAlivePingTime'] = time();
                            $clients[$j]['loops'] = 1;
                        } else {

                            $clients[$j]['loops'] = $clients[$j]['loops'] + 1;
                            if ( //!isset($clients[$j]['lastKeepAlivePingTime']) ||
                                //empty($clients[$j]['lastKeepAlivePingTime']) ||
                                (time() - $clients[$j]['lastKeepAlivePingTime']) >= $keepAlivePingTimeout) {

                                echo('Server is looping, sending keepalive...' . "\r\n");
                                if (isset($clients[$j]['lastKeepAlivePingTime']) && !empty($clients[$j]['lastKeepAlivePingTime'])) {
                                    $diff = time() - $clients[$j]['lastKeepAlivePingTime'];
                                    //e('Time Diff...' . (string)$diff . "\r\n");
                                }

                                $clients[$j]['lastKeepAlivePingTime'] = time();
                                if (!socket_write($clients[$j]['socket'], 'KeepAlive: Ping' . "\r\n")) {
                                    echo 'Client ', $j, ' not found, killing...', "\n";
                                    socket_close($clients[$j]['socket']);
                                    unset($clients[$j]);
                                    //die("Client was not found. Terminated...");
                                }

                            }
                        }


                        // Here we will check actions from storage (e.g. database, memcache or redis...)
                        // and send message accordingly if it exists
                        if (!isset($clients[$j]['sendActionOnlyOnce']) || !$clients[$j]['sendActionOnlyOnce']) {
                            $username = $clients[$j]['username'];

                            $action = getCurrentCubeAction($username);
                            sleep(1);

                            if (isset($action['action']) && $action['params']) {
                                $actionToSend = formatAction($action['action'], $action['params']);

                                if (!socket_write($clients[$j]['socket'], $actionToSend . "\r\n")) {
                                    echo 'Client ', $j, ' not found, killing...', "\n";
                                    socket_close($clients[$j]['socket']);
                                    unset($clients[$j]);
                                    //die("Client was not found. Terminated...");
                                }
                                else {
                                    $clients[$j]['sendActionOnlyOnce'] = true;
                                    e('Sent action request to client '. $clients[$j]['username']);
                                }
                            }
                            else {
                                e('Currently there are no one actions for cube '. $clients[$j]['username']);
                            }
                        }
                        else {
                            e('Action test example. We have already send one action for cube '. $clients[$j]['username']);
                        }

                    }
                }
            }

    }

}

socket_close($socket);

